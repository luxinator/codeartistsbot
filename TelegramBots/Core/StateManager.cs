﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBots.Core
{
    public class StateManager<T> : IStateManager<T>
        where T : IChatState, new()
    {
        protected Dictionary<long, T> states = new Dictionary<long, T>();
        private readonly ITelegramApi telegramApi;
        private readonly ICommandParser commandParser;

        public StateManager(ITelegramApi telegramApi, ICommandParser commandParser)
        {
            this.telegramApi = telegramApi;
            this.commandParser = commandParser;
            LoadStates();
        }

        public async Task<T> HandleState(long chatId, string chatName, string botName, string message)
        {
            var commands = commandParser.Parse(botName, message);

            if (!states.ContainsKey(chatId))
            {
                if (commands.command == "start")
                {
                    var state = new T();
                    state.ChatId = chatId;
                    state.Name = chatName;
                    states.Add(chatId, state);
                    SaveStates();
                    await telegramApi.SendTextMessageAsync(chatId, $"Started {botName} for {chatName}");
                }
            }
            else if (commands.command == "stop")
            {
                states.Remove(chatId);
                SaveStates();
                await telegramApi.SendTextMessageAsync(chatId, $"Stopped {botName} for {chatName}");
            }
            else if (commands.command == "status")
            {
                var status = GetStatus();
                await telegramApi.SendTextMessageAsync(chatId, status);
            }
            else
            {
                return states[chatId];
            }

            return default(T);
        }

        public void SaveStates()
        {
            var fileName = typeof(T).Name + ".json";

            var path = Path.Combine("config", fileName);
            var result = JsonConvert.SerializeObject(states);
            File.WriteAllText(path, result);
        }

        public void LoadStates()
        {
            var fileName = typeof(T).Name + ".json";

            var path = Path.Combine("config", fileName);
            if (File.Exists(path))
            {
                var result = File.ReadAllText(path);
                states = JsonConvert.DeserializeObject<Dictionary<long, T>>(result);
            }
        }

        private string GetStatus()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"Bot has {states.Count} states.");
            stringBuilder.AppendLine();

            foreach (var state in states)
            {
                stringBuilder.AppendLine(state.Value.ToString());
            }

            return stringBuilder.ToString();
        }

        public IEnumerable<T> GetStates() => states.Values;
    }
}
