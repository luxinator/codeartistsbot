﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using TelegramBots;

namespace CodeArtistsBot
{
    public class WebHookController : Controller
    {
        private readonly ITelegramBots telegramBots;

        public WebHookController(ITelegramBots telegramBots)
        {
            this.telegramBots = telegramBots;
        }

        [HttpPost]
        public async Task<IActionResult> Index([FromBody] Update update)
        {
            try
            {
                if (update?.Message != null && update.Message.Type == Telegram.Bot.Types.Enums.MessageType.TextMessage)
                    await telegramBots.HandleMessage(new TelegramMessage
                    {
                        Text = update.Message.Text, 
                        ChatId = update.Message.Chat.Id,
                        From = update.Message.From.FirstName + " " + update.Message.From.LastName,
                        UserId = update.Message.From.Id, 
                    });
            }
            catch (Exception ex) {
                return Json(new
                {
                    err = ex.ToString()
                });
            }

            return Json(new {
                
            });
        }
    }
}
