﻿namespace TelegramBots.Core
{
    public interface ICommandParser
    {
        (string command, string value) Parse(string botName, string message);
    }
}
