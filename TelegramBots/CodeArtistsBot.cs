﻿using TelegramBots.Core;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TelegramBots
{
    public class CodeArtistsBot : ITelegramBot
    {
        private readonly ITelegramApi telegramApi;
        private readonly ICommandParser commandParser;
        private readonly IStateManager<CodeArtistsBotState> stateManager;

        public CodeArtistsBot(ITelegramApi telegramApi, ICommandParser commandParser, IStateManager<CodeArtistsBotState> stateManager)
        {
            this.telegramApi = telegramApi;
            this.commandParser = commandParser;
            this.stateManager = stateManager;
        }

        public string Name => nameof(CodeArtistsBot);

        public string ShortName => "ca";

        public async Task HandleMessage(TelegramMessage telegramMessage)
        {
            var chatId = telegramMessage.ChatId;
            var message = telegramMessage.Text;
            var botName = ShortName ?? Name;

            var chatState = await stateManager.HandleState(chatId, telegramMessage.ChatTitle, botName, message);
            if (chatState == null)
                return;

            var commandValues = commandParser.Parse(botName, message);
            if (commandValues.command != null)
            {
                var cmd = commandValues.command;
                var value = commandValues.value;
                string response = null;

                switch (cmd)
                {
                    case nameof(CodeArtistsBotState.EnablePublish):
                    case "pub":
                        if (bool.TryParse(value, out var boolEnablePublish))
                        {
                            chatState.EnablePublish = boolEnablePublish;
                            response = $"{nameof(CodeArtistsBotState.EnablePublish)} was set to {value}";
                            stateManager.SaveStates();
                        }
                        break;

                    case nameof(CodeArtistsBotState.EnableSubscribe):
                    case "sub":
                        if (bool.TryParse(value, out var boolEnableSubscribe))
                        {
                            chatState.EnableSubscribe = boolEnableSubscribe;
                            response = $"{nameof(CodeArtistsBotState.EnableSubscribe)} was set to {value}";
                            stateManager.SaveStates();
                        }
                        break;
                }

                if (!string.IsNullOrEmpty(response))
                    await telegramApi.SendTextMessageAsync(chatId, response);
            }
            else
            {
                await HandleMessage(chatState, telegramMessage);
            }
        }

        private async Task HandleMessage(CodeArtistsBotState chatState, TelegramMessage telegramMessage)
        {
            if (!chatState.EnablePublish)
                return;

            // https://weblogs.asp.net/farazshahkhan/regex-to-find-url-within-text-and-make-them-as-link
            var regex = new Regex("http(s)?://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);

            //bool isUri = Uri.IsWellFormedUriString(telegramMessage.Text, UriKind.Absolute);
            if (regex.IsMatch(telegramMessage.Text))
            {
                var sendMessageTexts = new List<Task>();
                foreach (var state in stateManager.GetStates())
                {
                    if (state.EnableSubscribe)
                    {
                        sendMessageTexts.Add(telegramApi.ForwardMessageAsync(
                            state.ChatId, telegramMessage.ChatId, telegramMessage.MessageId));
                    }
                }

                await Task.WhenAll(sendMessageTexts.ToArray());
            }
        }

        public async void Help(long chatId)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"Help for bot /{Name}:");
            stringBuilder.AppendLine($"{Name} can be your friend when you are lonely");
            stringBuilder.AppendLine();

            stringBuilder.AppendLine($"Status: e.g. /{ShortName ?? Name} status");
            stringBuilder.AppendLine($"Gives status info about the number of conversations and associated states.");
            stringBuilder.AppendLine();

            stringBuilder.AppendLine($"Api: e.g. /{ShortName ?? Name} api");
            stringBuilder.AppendLine($"Returns the webhook endpoint.");
            stringBuilder.AppendLine();

            stringBuilder.AppendLine($"Start: e.g. /{ShortName ?? Name} start");
            stringBuilder.AppendLine($"Enables {Name} for the conversation");
            stringBuilder.AppendLine();

            stringBuilder.AppendLine($"Stop: e.g. /{ShortName ?? Name} stop");
            stringBuilder.AppendLine($"Disables {Name} for the conversation. State will be deleted.");
            stringBuilder.AppendLine();

            stringBuilder.AppendLine($"{nameof(CodeArtistsBotState.EnablePublish)}: e.g. /{ShortName ?? Name} {nameof(CodeArtistsBotState.EnablePublish)} true");
            stringBuilder.AppendLine($"Enables or disables publishing of all urls in this conversation.");
            stringBuilder.AppendLine();

            stringBuilder.AppendLine($"{nameof(CodeArtistsBotState.EnableSubscribe)}: e.g. /{ShortName ?? Name} {nameof(CodeArtistsBotState.EnableSubscribe)} true");
            stringBuilder.AppendLine($"Enables or disables subscriptions for all urls published to {Name}.");
            stringBuilder.AppendLine();

            await telegramApi.SendTextMessageAsync(chatId, stringBuilder.ToString());
        }

    }

    public class CodeArtistsBotState : IChatState
    {
        public bool EnablePublish { get; set; }

        public bool EnableSubscribe { get; set; }

        public long ChatId { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return Name + "\r\n" +
                $"{nameof(EnablePublish)} = {EnablePublish} " +
                $"{nameof(EnableSubscribe)} = {EnableSubscribe}";
        }
    }
}
