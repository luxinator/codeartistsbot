﻿using System.Threading.Tasks;
using Telegram.Td.Api;

namespace TelegramClient
{
    public interface ITDLibClient
    {
        Task Load();

        void OnAuthorizationStateUpdated(AuthorizationState authorizationState);

        Task<TBaseObject> Get<TFunction, TBaseObject>(TFunction function)
          where TFunction : Function
          where TBaseObject : class, BaseObject;

        Task SendMessage(long chatId, string message);
    }
}
