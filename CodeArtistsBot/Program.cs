﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Net;

namespace CodeArtistsBot
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting");

            BuildWebHost(args).Run();

            Console.WriteLine("Stopped");
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel(options =>
                {
                    options.Listen(IPAddress.Any, 8443, listenOptions =>
                    {
                        listenOptions.UseHttps("telegram.appbyfex.com.pfx", "telegram.appbyfex.com");
                    });
                })
                .Build();
    }
}
