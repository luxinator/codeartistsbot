﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Td;
using Telegram.Td.Api;
using Td = Telegram.Td;
using TdApi = Telegram.Td.Api;

namespace TelegramClient
{
    public class TDLibClient : ITDLibClient
    {
        private static Td.Client _client = null;
        private static AuthorizationState _authorizationState = null;
        private static readonly string _newLine = Environment.NewLine;
        private readonly ClientResultHandler clientResultHandler;
        private static volatile string _currentPrompt = null;
        private static volatile AutoResetEvent _gotAuthorization = new AutoResetEvent(false);
        private static volatile bool _quiting = false;

        public TDLibClient(ClientResultHandler clientResultHandler)
        {
            this.clientResultHandler = clientResultHandler;
        }

        private static Td.Client CreateTdClient(ClientResultHandler clientResultHandler)
        {
            Td.Client result = Td.Client.Create(clientResultHandler);
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                result.Run();
            }).Start();
            return result;
        }

        private static void Print(string str)
        {
            if (_currentPrompt != null)
            {
                Console.WriteLine();
            }
            Console.WriteLine(str);
            if (_currentPrompt != null)
            {
                Console.Write(_currentPrompt);
            }
        }

        private static string ReadLine(string str)
        {
            Console.Write(str);
            _currentPrompt = str;
            var result = Console.ReadLine();
            _currentPrompt = null;
            return result;
        }

        public void OnAuthorizationStateUpdated(TdApi.AuthorizationState authorizationState)
        {
            if (authorizationState != null)
            {
                _authorizationState = authorizationState;
            }
            if (_authorizationState is TdApi.AuthorizationStateWaitTdlibParameters)
            {
                TdApi.TdlibParameters parameters = new TdApi.TdlibParameters();
                parameters.DatabaseDirectory = "tdlib";
                parameters.UseMessageDatabase = true;
                parameters.UseSecretChats = true;
                parameters.ApiId = 94575;
                parameters.ApiHash = "a3406de8d171bb422bb6ddf3bbd800e2";
                parameters.SystemLanguageCode = "en";
                parameters.DeviceModel = "Desktop";
                parameters.SystemVersion = "Unknown";
                parameters.ApplicationVersion = "1.0";
                parameters.EnableStorageOptimizer = true;

                _client.Send(new TdApi.SetTdlibParameters(parameters), new AuthorizationRequestHandler());
            }
            else if (_authorizationState is TdApi.AuthorizationStateWaitEncryptionKey)
            {
                _client.Send(new TdApi.CheckDatabaseEncryptionKey(), new AuthorizationRequestHandler());
            }
            else if (_authorizationState is TdApi.AuthorizationStateWaitPhoneNumber)
            {
                string phoneNumber = ReadLine("Please enter phone number: ");
                _client.Send(new TdApi.SetAuthenticationPhoneNumber(phoneNumber, false, false), new AuthorizationRequestHandler());
            }
            else if (_authorizationState is TdApi.AuthorizationStateWaitCode)
            {
                string code = ReadLine("Please enter authentication code: ");
                _client.Send(new TdApi.CheckAuthenticationCode(code, "", ""), new AuthorizationRequestHandler());
            }
            else if (_authorizationState is TdApi.AuthorizationStateWaitPassword)
            {
                string password = ReadLine("Please enter password: ");
                _client.Send(new TdApi.CheckAuthenticationPassword(password), new AuthorizationRequestHandler());
            }
            else if (_authorizationState is TdApi.AuthorizationStateReady)
            {
                //_haveAuthorization = true;
                _gotAuthorization.Set();
            }
            else if (_authorizationState is TdApi.AuthorizationStateLoggingOut)
            {
                //_haveAuthorization = false;
                Print("Logging out");
            }
            else if (_authorizationState is TdApi.AuthorizationStateClosing)
            {
                //_haveAuthorization = false;
                Print("Closing");
            }
            else if (_authorizationState is TdApi.AuthorizationStateClosed)
            {
                Print("Closed");
                if (!_quiting)
                {
                    //_client = CreateTdClient(); // recreate _client after previous has closed
                }
            }
            else
            {
                Print("Unsupported authorization state:" + _newLine + _authorizationState);
            }
        }

        private class AuthorizationRequestHandler : Td.ClientResultHandler
        {
            void Td.ClientResultHandler.OnResult(TdApi.BaseObject @object)
            {
                if (@object is TdApi.Error)
                {
                    Print("Receive an error:" + _newLine + @object);
                    // OnAuthorizationStateUpdated(null); // repeat last action
                }
                else
                {
                    // result is already received through UpdateAuthorizationState, nothing to do
                }
            }
        }

        //private class UpdatesHandler : Td.ClientResultHandler
        //{
        //    void Td.ClientResultHandler.OnResult(TdApi.BaseObject @object)
        //    {
        //        if (@object is TdApi.UpdateAuthorizationState)
        //        {
        //            OnAuthorizationStateUpdated((@object as TdApi.UpdateAuthorizationState).AuthorizationState);
        //        }
        //        else if (@object is TdApi.UpdateNewMessage updateNewMessage)
        //        {
        //            if (updateNewMessage.Message.Content is MessageText messageText)
        //            {
        //                var chatId = updateNewMessage.Message.ChatId;
        //                var from = updateNewMessage.Message.SenderUserId;
        //                var message = messageText.Text.Text.ToString();
        //                Print($"{chatId} {from} {message}");

        //            }
        //            // Print("Unsupported update: " + @object);
        //        }
        //    }
        //}

        private class DefaultEventHandler<TBaseObject> : Td.ClientResultHandler
            where TBaseObject : class, BaseObject
        {
            private readonly AutoResetEvent autoResetEvent = new AutoResetEvent(false);

            public AutoResetEvent AutoResetEvent => autoResetEvent;

            public TBaseObject Result { get; set; }

            void Td.ClientResultHandler.OnResult(TdApi.BaseObject @object)
            {
                if (@object is TBaseObject baseObject)
                {
                    Result = baseObject;
                    autoResetEvent.Set();
                }
                else
                {
                    autoResetEvent.Set();
                    // throw new Exception(@object.ToString());
                }
            }
        }

        public async Task<TBaseObject> Get<TFunction, TBaseObject>(TFunction function)
          where TFunction : Function
          where TBaseObject : class, BaseObject
        {
            var handler = new DefaultEventHandler<TBaseObject>();

            await Task.Run(() =>
            {
                _client.Send(function, handler);
                handler.AutoResetEvent.WaitOne();
            });

            return handler.Result;
        }

        public async Task SendMessage(long chatId, string message)
        {
            // initialize reply markup just for testing
            TdApi.InlineKeyboardButton[] row = { new TdApi.InlineKeyboardButton("https://telegram.org?1", new TdApi.InlineKeyboardButtonTypeUrl()), new TdApi.InlineKeyboardButton("https://telegram.org?2", new TdApi.InlineKeyboardButtonTypeUrl()), new TdApi.InlineKeyboardButton("https://telegram.org?3", new TdApi.InlineKeyboardButtonTypeUrl()) };
            TdApi.ReplyMarkup replyMarkup = new TdApi.ReplyMarkupInlineKeyboard(new TdApi.InlineKeyboardButton[][] { row, row, row });

            TdApi.InputMessageContent content = new TdApi.InputMessageText(new TdApi.FormattedText(message, null), false, true);

            await Get<SendMessage, BaseObject>(new TdApi.SendMessage(chatId, 0, false, false, replyMarkup, content));
        }

        public async Task Load()
        {
            // disable TDLib log
            Td.Log.SetVerbosityLevel(0);
            if (!Td.Log.SetFilePath("tdlib.log"))
            {
                throw new System.IO.IOException("Write access to the current directory is required");
            }

            await Task.Run(() =>
            {
                _client = CreateTdClient(clientResultHandler);
                _gotAuthorization.Reset();
                _gotAuthorization.WaitOne();
                Print("Authorization complete");
            });
        }
    }
}
