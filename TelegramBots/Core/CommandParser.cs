﻿using System.Linq;
using System.Text.RegularExpressions;

namespace TelegramBots.Core
{
    public class CommandParser : ICommandParser
    {
        public (string command, string value) Parse(string botName, string message)
        {
            var regex = new Regex($@"\/{botName}\s(\w+)\s?([\w\.]+)?\s?(\w+)?", RegexOptions.IgnoreCase);
            if (regex.IsMatch(message))
            {
                var matches = regex.Match(message);
                var result = matches.Groups
                    .Cast<Group>()
                    .Select(g => g.Value)
                    .ToArray();

                return (result[1], result[2]);
            }
            else
            {
                return (null, null);
            }
        }
    }
}
