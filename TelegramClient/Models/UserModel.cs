﻿namespace TelegramClient
{
    namespace TelegramClient.Models
    {
        public class UserModel
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
    }
}
