﻿using System.Threading.Tasks;

namespace TelegramBots
{
    public interface ITelegramApi
    {
        string ApiKey { get; }

        Task SendTextMessageAsync(long chatId, string message);

        Task DeleteMessageAsync(long chatId, long messageId);

        Task SendVideoAsync(long chatId, string url, string message);

        Task ForwardMessageAsync(long chatId, long id, long messageId);
    }
}
