﻿using Microsoft.Extensions.DependencyInjection;
using Telegram.Td;
using TelegramBots;
using TelegramBots.Clients;
using TelegramBots.Core;

namespace TelegramClient
{
    public static class IoC
    {
        public static ServiceProvider RegisterServices(ClientResultHandler clientResultHandler)
        {
            IServiceCollection services = new ServiceCollection();
            services.AddSingleton<ITDLibClient, TDLibClient>();

            // Core
            services.AddSingleton<ITelegramApi, TelegramDesktopAPI>();
            services.AddSingleton<ITelegramBots, TelegramBots.TelegramBots>();
            services.AddSingleton<ICommandParser, CommandParser>();

            // States
            services.AddSingleton<IStateManager<CodeArtistsBotState>, StateManager<CodeArtistsBotState>>();
            services.AddSingleton<IStateManager<TodoBotState>, StateManager<TodoBotState>>();
            services.AddSingleton<IStateManager<MemeBotState>, StateManager<MemeBotState>>();
            services.AddSingleton<IStateManager<HappinessBotState>, StateManager<HappinessBotState>>();

            // Clients
            services.AddSingleton<IFavQsClient, FavQsClient>();
            services.AddSingleton<IGiphyClient, GiphyClient>();

            // Bots
            services.AddSingleton<ITelegramBot, TelegramBots.CodeArtistsBot>();
            services.AddSingleton<ITelegramBot, TodoBot>();
            services.AddSingleton<ITelegramBot, MemeBot>();
            services.AddSingleton<ITelegramBot, HappinessBot>();

            services.AddSingleton(typeof(ClientResultHandler), clientResultHandler);

            return services.BuildServiceProvider();
        }

    }
}
