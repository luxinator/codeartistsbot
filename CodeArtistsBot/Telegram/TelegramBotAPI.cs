﻿using System;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramBots;

namespace CodeArtistsBot
{
    public class TelegramBotAPI : ITelegramApi
    {
        private const string BOTAPI = "487291985:AAGMfeJAthx43zelVz4NepN346a_TyvH440";

        private static readonly ITelegramBotClient _api = new TelegramBotClient(BOTAPI);

        public ITelegramBotClient Api => _api;

        public string ApiKey { get; } = BOTAPI;

        public Task DeleteMessageAsync(long chatId, long messageId)
        {
            return _api.DeleteMessageAsync(chatId, (int)messageId);
        }

        public Task ForwardMessageAsync(long chatId, long fromChatId, long messageId)
        {
            return _api.ForwardMessageAsync(chatId, fromChatId, (int)messageId);
        }

        public Task SendTextMessageAsync(long chatId, string message)
        {
            return _api.SendTextMessageAsync(chatId, message);
        }

        public Task SendVideoAsync(long chatId, string url, string message)
        {
            return _api.SendVideoAsync(chatId, new FileToSend(new Uri(url)), caption: message);
        }
    }
}
