﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Td;
using Telegram.Td.Api;
using TelegramBots;
using TelegramClient.TelegramClient.Models;
using TdApi = Telegram.Td.Api;

namespace TelegramClient
{
    class Program : ClientResultHandler
    {
        private readonly Dictionary<long, ChatModel> _chats = new Dictionary<long, ChatModel>();
        private readonly ITelegramBots bots;
        private readonly ITDLibClient tdlibClient;

        public Program()
        {
            var serviceProvider = IoC.RegisterServices(this);
            tdlibClient = serviceProvider.GetService<ITDLibClient>();
            bots = serviceProvider.GetService<ITelegramBots>();
        }

        public static async Task Main(string[] args)
        {
            var p = new Program();
            await p.Load();
            Console.ReadLine();
        }

        private async Task Load()
        {
            await tdlibClient.Load();
            await GetChats();
        }

        public void OnResult(BaseObject baseObject)
        {
            if (baseObject is TdApi.UpdateAuthorizationState updateAuthorizationState)
            {
                tdlibClient.OnAuthorizationStateUpdated(updateAuthorizationState.AuthorizationState);
            }
            else if (baseObject is TdApi.UpdateNewMessage updateNewMessage)
            {
                if (updateNewMessage.Message.Content is MessageText messageText)
                {
                    var chatId = updateNewMessage.Message.ChatId;
                    var from = updateNewMessage.Message.SenderUserId;
                    var message = messageText.Text.Text.ToString();

                    var chat = _chats.ContainsKey(chatId)
                        ? _chats[chatId]
                        : null;

                    bots.HandleMessage(new TelegramMessage()
                    {
                        ChatId = chatId,
                        Text = message,
                        From = chat?.Users.ContainsKey(from) ?? false
                            ? chat.Users[from].Name
                            : from.ToString(),
                        UserId = from,
                        MessageId = updateNewMessage.Message.Id,
                        ChatTitle = chat?.Title
                    });
                }
            }
        }

        private async Task GetChats()
        {
            var chats = await tdlibClient.Get<GetChats, Chats>(new GetChats(Int64.MaxValue, 0, 100));
            foreach (var chatId in chats.ChatIds)
            {
                var chat = await tdlibClient.Get<GetChat, Chat>(new GetChat(chatId));
                _chats[chatId] = new ChatModel
                {
                    Id = chatId,
                    Title = chat.Title,
                    Users = new Dictionary<long, UserModel>()
                };

                var members = await tdlibClient.Get<SearchChatMembers, ChatMembers>(new SearchChatMembers(chatId, string.Empty, 50));
                if (members != null)
                {
                    foreach (var member in members.Members)
                    {
                        var user = await tdlibClient.Get<GetUser, User>(new GetUser(member.UserId));
                        _chats[chatId].Users.Add(
                            user.Id,
                            new UserModel()
                            {
                                Id = user.Id,
                                Name = user.FirstName + " " + user.LastName
                            });
                    }
                }
            }
        }
    }
}
