﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using TelegramBots;
using TelegramBots.Clients;
using TelegramBots.Core;

namespace CodeArtistsBot
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .Build();

            services.AddMvc();

            // Core
            services.AddSingleton<ITelegramApi, TelegramBotAPI>();
            services.AddSingleton<ITelegramBots, TelegramBots.TelegramBots>();
            services.AddSingleton<ICommandParser, CommandParser>();

            // States
            services.AddSingleton<IStateManager<CodeArtistsBotState>, StateManager<CodeArtistsBotState>>();
            services.AddSingleton<IStateManager<TodoBotState>, StateManager<TodoBotState>>();
            services.AddSingleton<IStateManager<MemeBotState>, StateManager<MemeBotState>>();
            services.AddSingleton<IStateManager<HappinessBotState>, StateManager<HappinessBotState>>();

            // Clients
            services.AddSingleton<IFavQsClient, FavQsClient>();
            services.AddSingleton<IGiphyClient, GiphyClient>();

            // Bots
            services.AddSingleton<ITelegramBot, TelegramBots.CodeArtistsBot>();
            services.AddSingleton<ITelegramBot, TodoBot>();
            services.AddSingleton<ITelegramBot, MemeBot>();
            services.AddSingleton<ITelegramBot, HappinessBot>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/WebHook/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=WebHook}/{action=Index}/{id?}");
            });
        }
    }
}
