﻿using System.Collections.Generic;

namespace TelegramClient
{
    namespace TelegramClient.Models
    {
        public class ChatModel
    {
        public long Id { get; set; }

        public Dictionary<long, UserModel> Users { get; set; }

        public string Title { get; set; }
    }
    }
}
