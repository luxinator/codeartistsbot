﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Td.Api;
using TelegramBots;
using TelegramClient.TelegramClient.Models;

namespace TelegramClient
{
    public class TelegramDesktopAPI : ITelegramApi
    {
        private readonly ITDLibClient telegramClient;

        public TelegramDesktopAPI(ITDLibClient telegramClient)
        {
            this.telegramClient = telegramClient;
        }

        public string ApiKey => throw new NotImplementedException();

        public Task DeleteMessageAsync(long chatId, long messageId)
        {
            return telegramClient.Get<DeleteMessages, BaseObject>(new DeleteMessages(chatId, new[] { messageId}, false));
        }

        public Task ForwardMessageAsync(long chatId, long id, long messageId)
        {
            return telegramClient.Get<ForwardMessages, BaseObject>(new ForwardMessages(chatId, id, new[] { messageId }, false, false, false));
        }


        public Task SendTextMessageAsync(long chatId, string message)
        {
            return telegramClient.SendMessage(chatId, message);
        }

        public Task SendVideoAsync(long chatId, string url, string message)
        {
            return telegramClient.SendMessage(chatId, $"{message}: {url}");
        }
    }
}
